// ES6
import { Gitlab, Types } from '@gitbeaker/node'; // All Resources
//import { Types } from '@gitbeaker/node';
import { config } from 'dotenv'
//require('dotenv').config()
config()


const api = new Gitlab({
  token: process.env["GITLAB_TOKEN_ADMIN"],
  host: process.env["GITLAB_HOST"],
})

function exitIfError(error, topic) {
  if(error) { console.log(`😡[${topic}]`, error.message); process.exit(1); }
}


// let projects = await api.Projects.all({ maxPages: 2, perPage: 40 });


/*
Inside a main group (for example a corporation)
Create a sub-group that represents a "group project"
Then we'll create a "pm-management" project (repository)
*/
// create the group project
async function createProjectGroup(name, path, description) {
  try {
    let projectGroup = await api.Groups.create(name, path, {
      description: description,
      visibility: "public",
      parent_id: process.env["PARENT_GROUP_ID"]
    })

    return [null, projectGroup]
    
  } catch (error) {
    return [error, null]
  }
}

async function createManagementProject(name, groupId) {
  console.log("🤖", name, groupId)
  try {
    let project = await api.Projects.create({
      name: name,
      visibility: "public",
      initialize_with_readme: true,
      namespace_id: groupId
    })
    //console.log(project)

    return [null, project]
    
  } catch (error) {
    return [error, null]
  }
}


var error
let projectGroup

[error, projectGroup] = await createProjectGroup("death-star-013", "death-star-013", "this is my project")
exitIfError(error, "group")

console.log("🙂[group]", projectGroup.id, projectGroup.name, projectGroup.description)

let managementProject
[error, managementProject]  = await createManagementProject("management", projectGroup.id)
exitIfError(error, "management project")

console.log("🙂[management project]", managementProject.id, managementProject.name)

