// ES6
import { Gitlab, Types } from '@gitbeaker/node'; // All Resources
//import { Types } from '@gitbeaker/node';


const api = new Gitlab({
  token: process.env["GITLAB_TOKEN_ADMIN"],
  host: "https://gitlab.com",
})


// delete the group project
//https://gitlab.com/tanuki-workshops/acme-corp
api.Groups.remove("tanuki-workshops/acme-corp/death-star-007").then(result => {
  console.log("🙂", result)
}).catch(error => {
  console.log("😡", error)
})


api.Groups.remove("tanuki-workshops/acme-corp/death-star-008").then(result => {
  console.log("🙂", result)
}).catch(error => {
  console.log("😡", error)
})

api.Groups.remove("tanuki-workshops/acme-corp/death-star-011").then(result => {
  console.log("🙂", result)
}).catch(error => {
  console.log("😡", error)
})

api.Groups.remove("tanuki-workshops/acme-corp/death-star-012").then(result => {
  console.log("🙂", result)
}).catch(error => {
  console.log("😡", error)
})

api.Groups.remove("tanuki-workshops/acme-corp/death-star-013").then(result => {
  console.log("🙂", result)
}).catch(error => {
  console.log("😡", error)
})


